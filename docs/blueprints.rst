Blueprints
==========

post-receive git hook for AUR packages
--------------------------------------

1. generate a bare repository on an Arch Linux instance
2. add this file as ``hooks/post-receive``

   Example for fpyutils

   .. include:: includes/post-receive
      :code: shell
      :literal:


3. add this file as ``hooks/post-receive.conf`` for the configuration

   Example for fpyutils

   .. include:: includes/post-receive.conf
      :code: shell
      :literal:

Makefile
--------

Example for fpyutils

.. include:: includes/Makefile
   :code: makefile
   :literal:
